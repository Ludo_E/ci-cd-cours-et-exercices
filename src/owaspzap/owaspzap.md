# Tests de sécurité

Pour mettre en place des tests de sécurités et trouver les vulnérabilités d'une application nous allons utiliser le scanner Owasp ZAP.

![Logo du scanner de vulnérabilités Owasp ZAP](./images/owasp-logo.jpg)

## Qu'est-ce que ZAP ?

Owasp ZAP est un outil open source de "pentest" ("penetration testing") permettant de trouver les **vulnérabilités d'une application web**.

Il s'agit d'un des outils les plus utilisés quand il est question de tester la sécurité d'une application web.

ZAP permet de généer des rapports de sécurité complets en HTML, pdf 

Session management
Spider scan, AJAX spider, Active Scane

## ZAP avec l'application desktop

Avant d'intégrer ZAP dans le pipeline Jenkins, vous pouvez installer l'outil en local sous sa version ["ZAP Desktop"](https://www.zaproxy.org/download/).

Ceci vous permettra d'effectuer des tests de sécurité manuellemnet sur vos applications.

## ZAP basé sur docker

Vous pouvez utiliser les images officielles docker pour utiliser ZAP : [https://www.zaproxy.org/docs/docker/about/]().

```bash
docker run -v ${pwd}/report/:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t http://host.docker.internal:8088/myapp -r report.html
```

Voici un descriptif des différents paramétres utilisés :
- `-v  ${pwd}/report/:/zap/wrk/:rw` : **(paramètre Docker)** permet de monter un volume permettant de récupérer les rapports de OWASP Zap.
`${pwd}/report/` est le dossier local dans lequel les rapports vont être recueillis.
`/zap/wrk/` le dossier du conteneur.
`:rw` indique que les fichiers/dossiers sont en **lecture et en écriture**

- `-t owasp/zap2docker-stable zap-baseline.py` : **(paramètre Docker)** permet de lancer un conteneur basé sur l'image **owasp/zap2docker-stable**. Le script **"zap-baseline.py"** est utilisé par le conteneur pour mener le test de sécurité.

- `-t http://host.docker.internal:8088/myapp ` : **(paramètre du conteneur)** indique l'URL de l'application à tester.

- `-r report.html` : **(paramètre du conteneur)** : indique le nom du fichier de rapport.

```admnonish tip
L'approche en utilisant les images Docker est celle à utiliser pour l'automatisation dans un pipeline Jenkins.
```

## Processus de test

## Que tester ?

Première étape : tester en local et manuellement !

Plusieurs types de scans :
- rapide : baseline scan
- tests approfondis : full scan (peut durer plusieurs heures)
