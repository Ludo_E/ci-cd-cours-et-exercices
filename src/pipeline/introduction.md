# Pipeline Jenkin

Permet d'ordonancer les différentes phases de l'intégration et du déploiement continu.

Dans la suite de ce document vous allez apprendre à concevoir un pipeline permettant d'automatiser les phases suivantes :
1. Tests de qualité (SonarQube)
2. Déploiement sur un serveur
3. Tests de sécurité (OWasp Zap)
4. Tests de charge
5. Tests de performance


