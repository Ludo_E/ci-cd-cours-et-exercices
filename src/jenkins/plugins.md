# Premier lancement

## Login en administrateur
Lors du premier lancement il vous faudra vous identifier avec un mot de passe aléatoire générer par le système Jenkins.


```admonish question
Ce mot de passe est contenu dans un fichier que vous pouvez trouver dans le système de fichier de votre conteneur.

A vous de le retrouver !
```

## Plugins à installer

Lors du premier lancement, Jenkins vous demande si vous souhaitez installer des extensions, sélectionnez celles-ci :
- Git ;
- Git parameter ;
- Github ou Gitlab (en fonction du service que vous utilisez) ;
- SonarQube Scanner.

![Page d'installation des plugins Jenkins](./images/jenkins-plugins.png)

# Faire le lien entre Jenkins et votre dépôt Git

Jenkins va devoir entrer en communication avec des dépôts Git pour pouvoir lancer des opérations de compilation et de vérification de code.

Il va donc falloir lui indiquer une façon d'accéder à ces dépôts.

Vous trouverez des procédures pour deux services fournissant des dépôts Git gratuitement : Gitlab et Github.

## Procédure de génération de token

### Gitlab
Le lien entre Jenkins et Gitlab se fait en utilisant un "API token" généré à partir de Gitlab.

![Ajout d'un token sur Gitlab](./images/gitlab-token.gif)

## Ajout du token sur Jenkins
Une fois le token généré, il vous faut l'ajouter à Jenkins, au niveau des paramètres système.

### Gitlab

Pour Gitlab il vous faut paramétrer l'accès en utilisant un **"Secret Text"**, comme indiqué par le gif suivant :

![Ajout d'un token sur Jenkins](./images/jenkins-add-token.gif)
