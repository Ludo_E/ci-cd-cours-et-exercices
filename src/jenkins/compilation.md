# Les noeuds : des environnement de compilation et de test

Jenkins est un outil permettant d'automatiser le traitement de projets dans n'importe quel technologie.

Il est ainsi doté d'un **contrôleur** qui agit comme un chef d'orchestre et qui peut distribuer des ordres de compilation sur des **noeuds** (aussi appelé **"nodes" ou "agents"**).

Ces noeuds sont en fait des machines (virtuelles ou physiques) tournant sur un système d'exploitation particulier (souvent Linux).

L'objectif premier de Jenkins est donc de récupérer le code d'un **dépôt Git** et de faire des actions automatisées sur les noeuds.

![Figure présentant l'organisation de Jenkins pour la compilation](./images/compilation-node.svg)

```admonish warning
Dans la suite de ce cours nous utiliserons le noeud par défaut appelé "built-in" pour limiter la complexité de la mise en place.

Ceci n'est pas la façon la plus optimisée de procéder !
Le mieux est de créer un noeud dédiées aux actions de Jenkins sur un projet.
```

## Configuration du noeud "built-in"
Vous allez mettre en place un environnement de compilation et de test s'appliquant aux projets Java.

Jusqu'à présent vous avez basé vos projets sur plusieurs outils :
- le **JDK Java** fournissant le compilateur **javac** ainsi qu'un ensemble de librairies utilisée pour le développement.
- le gestionnaire de projet Java **Maven** (gestion des dépendances, automatisation des tests, gestion des options de compilation...).

Il va vous falloir configurer **chacun des outils de compilation** pour pouvoir automatiser les opérations Jenkins.

Vous allez donc :
- installer un JDK ;
- installer Maven.

### Installation du JDK

Pour installer un JDK, rendez-vous sur la page d'administration des outils Jenkins : **Tableau de bord > Administrer Jenkins > Tools**

Puis allez dans la partie JDK et paramétrez la section en vous inspirant de la capture suivante :
![Section de configuration de JDK](./images/add-jdk.svg)

```admonish warning
Attention au champ "sous-dossier d'extraction d'achive".

Il vous faut utiliser le nom du sous-dossier contenu dans l'archive "tar.gz" (cf. figure suivante).
```

![Section de configuration de JDK](./images/archive-jdk.PNG)

```admonish question
Installez le JDK en utilisant la version souhaitée.

[Exemple de site où trouver des archives de JDK](https://www.oracle.com/java/technologies/downloads/).
```

### Installation de Maven
Une fois le JDK installé, il vous faudra l'outil de gestion de projet Java, le fort sympathique **Maven**.

Même chose que pour le JDK, rendez-vous sur la page de paramétrage des outils : **Tableau de bord > Administrer Jenkins > Tools**

![Section de configuration de Maven](./images/add-maven.PNG)

Une fois toutes ces étapes effectuée, l'agent par défaut est équipé pour compiler vos projets Java.

