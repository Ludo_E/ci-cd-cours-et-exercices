1. Déploiement d'un docker local hébergeant SonarQube

Lancement de Sonar en local via Docker (dernière version de l'image) :
```
docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest
```

Pour se connecter :
- login : **admin**
- mot de passe : **admin**

2. Générer un token d'accès pour Jenkins

Afin que Jenkins (ou tout autre outil) puisse s'interfacer avec SonarQube, il faut générer un token d'accès.

Ceci peut-être effectué comme détaillé par le gif suivant :  
![Génération d'un token d'accès SonarQube](./images/sonarqube-token.gif)

```admonish warning
Bien faire attention de copier la chaîne de caractères représentant le token. Une fois la page quittée il n'est pas possible de la retrouver.
```

3. Créer un projet vierge





Temp :
Jenkins token : glpat-gMPjmxz1Pv4pNXAYi5sV